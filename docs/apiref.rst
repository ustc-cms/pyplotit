Reference
=========

YAML configuration parsing
--------------------------

A plotIt_ YAML configuration file should have the following structure:

.. code-block:: yaml

   configuration:
     # Configuration block
   files:
     file_name:
       # File block
     ...
   groups: # optional
     group_name:
       # Group block
     ...
   plots:
     plot_name:
       # Plot block
     ...
   systematics: # optional
     # just name (for shape) or name with systematic block
     ...
   legend: # optional
     # legend block

Such YAML files can be parsed with the :py:meth:`~plotit.plotit.loadFromYAML`
method.
It will return instances of the classes defined in
the :py:mod:`plotit.config` module, whose attribute listings below serve as
a reference of the allowed attributes in each block, and their types.

.. automethod:: plotit.plotit.loadFromYAML

.. automodule:: plotit.config
   :members:
   :undoc-members:


.. _plotIt: https://github.com/cp3-llbb/plotIt
